var axios = require('axios');

const KEY = 'edd7fd7dac31cb81df28f91455649911';
const PASSWORD = '330c304080eb8a70845b94ad0269bc50';
const SHARED_SECRET = 'a830be1921b796ad7eb9ad13ec16abdc';
const HOSTNAME = 'gointegrations-devtest.myshopify.com';
const URL = 'https://'+ KEY + ':' + PASSWORD + '@' + HOSTNAME + '/admin/products.json';
const TEST = './products.json';

module.exports = {
  getProducts: function() {
    var requestURL = TEST;
    return axios.get(requestURL).then(function(response) {
      
      return response.data.products;
    }, function(error) {
      throw new Error(error.response.data.message);
    });
  }
};
