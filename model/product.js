const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	id: {
		type: String,
		required: [true, 'id field is required and it has to be a string!']
	},
	title: {
		type: String,
		required: [true]
	},
	regularPrice: {
		type: String,
		required: [true]
	},
	image: {
		type: String,
		required: [true]
	},	
});

const Product = mongoose.model('product', ProductSchema);

module.exports = Product;

