var React = require('react');

var Product = React.createClass({

  render: function() {
    var {id, title, image, regularPrice} = this.props;
    return (
    `<div className="product product item text-center product-${id}">
      <img className="productImg" src="${image}"/>
      <h4 className="productName lineHeight-regular">${title}</h4>
      <p className="productPrice">$${regularPrice}</p>
      <button id="insert-${id}" className="addToCart">Add to Cart</button>	
	  </div>`
    );
  }
});

module.exports = Product;
