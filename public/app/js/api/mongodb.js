var axios = require('axios');

const URL = 'http://localhost:2000/products';

module.exports = {
  saveProduct: function(product) {
    var requestURL = URL;
    return axios.post(requestURL,product).then(function(response) {
      return response;
    }, function(error) {
      throw new Error(error.response.data.message);
    });
  }
};
