module.exports = {
	entry: './public/app/js/app.js',
	output: {
		path: __dirname + '/public/app/js/',
		filename: 'bundle.js'
	},
	watch: true,
	module: {
		loaders: [
			{
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'react']
				}
			}
		]
	},
	devtool: 'source-map'
};
