import React, { PropTypes } from 'react';

import Product from './Product';
import shopify from '../api/shopify';
import mongodb from '../api/mongodb';

var MainComponent = React.createClass({
 getIntitalState: function() {
    return {
      products: []
    }
  },
  componentWillMount: function() {
    var that = this;
    shopify.getProducts().then(function(products) {

      that.setState({
        products: products
      });

     // Save Product into mongodb
      that.state.products.map(function(product){
        var localProduct = {
          id: product.id,
          title: product.title,
          regularPrice: product.variants[0].price,
          image: product.image.src
        };
        mongodb.saveProduct(localProduct).then(function(res){
        });
      }); // saving data completed

    }); // pulling data completed
  },

  render: function() {
    var renderProduct = function() {
      this.state.products.map(function(product) {
        var localProduct = {
            id: product.id,
            title: product.title,
            regularPrice: product.variants[0].price,
            image: product.image.src
          };
        return <Product key={localProduct.id} {...localProduct}/>
      });
    };
    return (
      <div>

      </div>
    )
  }
});

export default MainComponent;
