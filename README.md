For the technical test we are going to ask you to connect to a Shopify eCommerce store using the Shopify restfulAPI. You will pull a collection of products and import them into a local datastore - an instance of MongoDB would be best but any data store would be acceptable. 

You will then show the products your own test website. This could be a locally hosted web site or a free or developer account on AWS would be better. 

You will price the products that you pulled from the collection at 0.75 x the retail price listed on shopify. That is you will offer the same product at a discount to what they are being offered on the other website. 

You will add a buy button for the products on your website which will push an order back into Shopify using the draft order API. 

You will save all of your code in a GitHub repository (we use bitbucket) that we can review. Hint…. Go ahead and use external packages to go faster. I’ve listed a yarn package below for shopify. 

Ideally we would like to see the project coded using with Node.js, MongoDB hosted on AWS. 

Here are some useful resources: 

https://help.shopify.com/api/reference 

https://yarnpkg.com/en/package/shopify-api-node 

https://aws.amazon.com/free/ 

https://bitbucket.org/product 

https://github.com/verekia/js-stack-from-scratch 

Shopify Store for Test: 
URL: https://gointegrations-devtest.myshopify.com/ 
Items in Collection: 3 

API key: edd7fd7dac31cb81df28f91455649911 
Password: 330c304080eb8a70845b94ad0269bc50 
Shared secret: a830be1921b796ad7eb9ad13ec16abdc 
URL format: https://apikey:password@hostname/admin/resource.json 
Example URL: https://edd7fd7dac31cb81df28f91455649911:330c304080eb8a70845b94ad0269bc50@gointegrations-devtest.myshopify.com/admin/products.json 



====================================================================================
Hi,

Here are details about today's exercise:

 Exercise Rules:
 - You have 30 minutes from the time you received our email to complete the exercise
 - You can use Google to look up anything you want.
 - You cannot ask anyone else for help.
 - Ignore or close the CSS and HTML panel. You should only focus on the JS panel.
 - Do not change any components marked 'do not change'
 - In the interest of confidentiality, please don't share this test.
 
 How To Write The test:
 - Visit this CodePen URL: http://codepen.io/pen?template=YpePWY
 - You can code in the JS panel and the app will live update.
 - These instructions will also appear there.
  
 What To Do When You are Done:
 - After 30 minutes, copy and paste all your code from this JS panel.
 - Reply to this email with the code.
 - Your reply email will be timestamped showing completion of the exercise in 30 minutes.
 
 What The Test App Should Do:
 - allow the user to select a product and quantity and add it to the products table
 - the table should display a subtotal for each order line
 - if the user enters the code PROMO10, a promotional discount of 10% should be applied product subtotals

 Example:
 - See this gif for a working example: http://i.imgur.com/UuLmVqp.gifv


Thanks and good luck!

