var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');

import MainComponent from './components/MainComponent';

ReactDOM.render(
    <MainComponent/>
   
  ,
  document.getElementById('app')
);
