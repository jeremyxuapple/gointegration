const express = require('express');
const router = express.Router();
const Product = require('../model/product');

router.get('/products', function(request, response, next) {
	Product.find({}).then(function(products) {
		response.send(products);
	});
});

router.post('/products', function(request, response, next) {
	
	Product.create(request.body).then(function(product) {
		response.send( product );
	}).catch(next);

	
});

module.exports = router;