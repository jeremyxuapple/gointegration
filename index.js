const express = require('express');
const routes = require('./routes/api');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

// Connect to mongodb
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/interview');

mongoose.connection.once('open', function() {
	console.log('MongoDB is connected...');
}).on('error', function(error) {
	console.log('error:', error);
});

app.use(express.static('public'));
// Parse body middleware
app.use(bodyParser.json());
// Initialize routes
app.use(routes);
// Error handle middleware
app.use(function(error, request, reponse, next) {
	reponse.status(422).send({error: error.message});
});

app.listen( process.env.port || 2000, function() {
	console.log("Now listening on port 2000...");
});


